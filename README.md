# Image-Analyzer #

Image-Analyzer is a very simple straight ideia of how to extract images metainformation. Our initial goal is extract dominant color and "temperatures" from images.

# Explanation #

Main ideias behind Image-Analyzer are explained on this [article](http://rdllopes.bitbucket.org/image-analyzer).

# Running #

There's already an shell script to help you run this app.

    usage: ./image-analyzer.sh [OPTIONS]
     -b,--block-size <SIZE>                 use SIZE blocks. (Default 1)
     -i,--input-file <INPUT_FILE>           use given file for analyze.
                                            (Required)
     -j,--join                              join colors using Pairwise
                                            Clustering Algorithm.
     -m,--max-cluster-distance <DISTANCE>   use DISTANCE to specify max
                                            cluster distance between colors
                                            clusters. (Default 12)
     -r,--result-file <RESULT_FILE>         the file which it will save the
                                            results. (Default result.html)

# Building #

This project is using [gradle](http://www.gradle.org/). Get gradle and just execute:

    gradle build


